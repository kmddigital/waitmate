var async = require('async');
var crypto = require('crypto');
var passport = require('passport');
var User = require('../models/User');
var Submission = require('../models/Submission');
var Email = require('./email');
var app = require('../app');
var reCAPTCHA = require('recaptcha2');

var logger = app.locals.logger;
var recaptcha = new reCAPTCHA({
    siteKey:'6LdT20AUAAAAAAC66KTg-ONVXwt6gjlAn79c5GZO',
    secretKey:'6LdT20AUAAAAAFnYmSZRB0lsNc5gO9Qt3_EIaYHr'
});


/**
 * Login required middleware
 */
exports.ensureAuthenticated = function(req, res, next)
{
    if (req.isAuthenticated())
    {
        next();
    }
    else
    {
        res.redirect('/login');
    }
};

/**
 * GET /login
 */
exports.loginGet = function(req, res)
{
    if (req.user)
    {
        return res.redirect('/');
    }
    res.render('login',
    {
        title: 'Log in'
    });
};

/**
 * POST /login
 */
exports.loginPost = function(req, res, next)
{
    req.assert('email', 'Email is not valid').isEmail();
    req.assert('email', 'Email cannot be blank').notEmpty();
    req.assert('password', 'Password cannot be blank').notEmpty();
    req.sanitize('email').normalizeEmail(
    {
        remove_dots: false
    });

    var errors = req.validationErrors();

    if (errors)
    {
        req.flash('error', errors);
        return isUndf(req.query.return) ? res.redirect('/login') : res.redirect('/login?return=' + req.query.return);
    }

    passport.authenticate('local', function(err, user, info)
    {
        if (!user)
        {
            req.flash('error', info);
            return isUndf(req.query.return) ? res.redirect('/login') : res.redirect('/login?return=' + req.query.return);
        }
        req.logIn(user, function(err)
        {
            if (!user.emailConfirmed && !user.emailConfirmationSent)
                confirmEmail(user);
            res.redirect(isUndf(req.query.return) ? '/' : req.query.return);
        });
    })(req, res, next);
};

/**
 * GET /logout
 */
exports.logout = function(req, res)
{
    req.logout();
    res.redirect('/');
};

/**
 * GET /signup
 */
exports.signupGet = function(req, res)
{
    if (req.user)
    {
        return res.redirect('/');
    }
    res.render('signup',
    {
        title: 'Sign up'
    });
};

/**
 * POST /signup
 */
exports.signupPost = function(req, res, next)
{
    recaptcha.validateRequest(req)
    .then(function() {
        req.assert('name', 'Name cannot be blank').notEmpty();
        req.assert('email', 'Email is not valid').isEmail();
        req.assert('email', 'Email cannot be blank').notEmpty();
        req.assert('password', 'Password must be at least 4 characters long').len(4);
        req.sanitize('email').normalizeEmail(
        {
            remove_dots: false
        });

        var errors = req.validationErrors();

        if (errors)
        {
            req.flash('error', errors);
            return res.redirect('/signup');
        }


        User.findOne(
        {
            email: req.body.email
        }, function(err, user)
        {
            if (user)
            {
                req.flash('error',
                {
                    msg: 'The email address you have entered is already associated with another account.'
                });
                return res.redirect('/signup');
            }
            user = new User(
            {
                name: req.body.name,
                email: req.body.email,
                password: req.body.password,
                emailConfirmationSent: true
            });
            user.save(function(err)
            {
                req.logIn(user, function(err)
                {
                    confirmEmail(user);
                    res.redirect('/');
                });
            });
        });
    })
    .catch(function(errorCodes) {
        let errors = { msg: 'Please complete the reCaptcha.' };
        req.flash('error', errors);
        return res.redirect('/signup');
    });
};

/**
 * GET /account
 */
exports.accountGet = function(req, res)
{
    res.render('profile',
    {
        title: 'My Account'
    });
};

/**
 * PUT /account
 * Update profile information OR change password.
 */
exports.accountPut = function(req, res, next)
{
    if ('password' in req.body)
    {
        req.assert('password', 'Password must be at least 8 characters long').len(8);
        req.assert('confirm', 'Passwords must match').equals(req.body.password);
    }
    else
    {
        req.assert('email', 'Email is not valid').isEmail();
        req.assert('email', 'Email cannot be blank').notEmpty();
        req.sanitize('email').normalizeEmail(
        {
            remove_dots: false
        });
    }

    var errors = req.validationErrors();

    if (errors)
    {
        req.flash('error', errors);
        return res.redirect('/account');
    }

    User.findById(req.user.id, function(err, user)
    {
        if ('password' in req.body)
        {
            user.password = req.body.password;
        }
        if (req.body.email && user.email !== req.body.email)
        {
            User.findOne({email: req.body.email}, function(err, cuser)
            {
                if (!cuser)
                {
                    user.emailConfirmed = false;
                    confirmEmail(user);
                    user.email = req.body.email;
                    user.name = req.body.name;
                    user.gender = req.body.gender;
                    user.location = req.body.location;
                    user.website = req.body.website;
                    user.save(function(err)
                    {
                        if ('password' in req.body)
                        {
                            req.flash('success',
                            {
                                msg: 'Your password has been changed.'
                            });
                        }
                        else if (err && err.code === 11000)
                        {
                            req.flash('error',
                            {
                                msg: 'The email address you have entered is already associated with another account.'
                            });
                        }
                        else
                        {
                            req.flash('success',
                            {
                                msg: 'Your profile information has been updated.'
                            });
                        }
                        res.redirect('/account');
                    });
                }
                else
                {
                    req.flash('error',
                    {
                        msg: 'The email address you have entered is already associated with another account.'
                    });
                    return res.redirect('/account');
                }
            });
        }
        else
        {
            user.name = req.body.name;
            user.gender = req.body.gender;
            user.location = req.body.location;
            user.website = req.body.website;
            if (req.body.sendConfirmationEmail)
            {
                confirmEmail(user);
            }
            user.save(function(err)
            {
                if ('password' in req.body)
                {
                    req.flash('success',
                    {
                        msg: 'Your password has been changed.'
                    });
                }
                else if (err && err.code === 11000)
                {
                    req.flash('error',
                    {
                        msg: 'The email address you have entered is already associated with another account.'
                    });
                }
                else
                {
                    req.flash('success',
                    {
                        msg: 'Your profile information has been updated.'
                    });
                }
                res.redirect('/account');
            });
        }
    });
};

/**
 * DELETE /account
 */
exports.accountDelete = function(req, res, next)
{
    User.remove(
    {
        _id: req.user.id
    }, function(err)
    {
        Submission.remove(
        {
            user: req.user.id
        }, function(err)
        {
            req.logout();
            req.flash('success',
            {
                msg: 'Your account has been permanently deleted.'
            });
            res.redirect('/');
        });
    });
};

/**
 * GET /unlink/:provider
 */
exports.unlink = function(req, res, next)
{
    User.findById(req.user.id, function(err, user)
    {
        switch (req.params.provider)
        {
            case 'facebook':
                user.facebook = undefined;
                break;
            case 'google':
                user.google = undefined;
                break;
            case 'twitter':
                user.twitter = undefined;
                break;
            case 'vk':
                user.vk = undefined;
                break;
            default:
                req.flash('error',
                {
                    msg: 'Invalid OAuth Provider'
                });
                return res.redirect('/account');
        }
        user.save(function(err)
        {
            req.flash('success',
            {
                msg: 'Your account has been unlinked.'
            });
            res.redirect('/account');
        });
    });
};

/**
 * GET /forgot
 */
exports.forgotGet = function(req, res)
{
    if (req.isAuthenticated())
    {
        return res.redirect('/');
    }
    res.render('forgot',
    {
        title: 'Forgot Password'
    });
};

/**
 * POST /forgot
 */
exports.forgotPost = function(req, res, next)
{
    req.assert('email', 'Email is not valid').isEmail();
    req.assert('email', 'Email cannot be blank').notEmpty();
    req.sanitize('email').normalizeEmail(
    {
        remove_dots: false
    });

    var errors = req.validationErrors();

    if (errors)
    {
        req.flash('error', errors);
        return res.redirect('/forgot');
    }

    async.waterfall([
        function(done)
        {
            crypto.randomBytes(16, function(err, buf)
            {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function(token, done)
        {
            User.findOne(
            {
                email: req.body.email
            }, function(err, user)
            {
                if (!user)
                {
                    req.flash('error',
                    {
                        msg: 'The email address ' + req.body.email + ' is not associated with any account.'
                    });
                    return res.redirect('/forgot');
                }
                user.passwordResetToken = token;
                user.passwordResetExpires = Date.now() + 3600000; // expire in 1 hour
                user.save(function(err)
                {
                    done(err, token, user);
                });
            });
        },
        function(token, user, done)
        {
            Email.sendPasswordReset(user.name, user.email, req.headers.host, token, (error, data) => {
                if (error)
                {
                    req.flash('error',
                    {
                        msg: 'An error occured when attempting to send password reset request email.'
                    });
                }
                else
                {
                    req.flash('info',
                    {
                        msg: 'An email has been sent to the requested email with further instructions.'
                    });
                }
                res.redirect('/forgot');
            });
        }
    ]);
};

/**
 * GET /reset
 */
exports.resetGet = function(req, res)
{
    if (req.isAuthenticated())
    {
        return res.redirect('/');
    }
    User.findOne({
        passwordResetToken: req.params.token
    })
    .where('passwordResetExpires').gt(Date.now())
    .exec(function(err, user)
    {
        if (!user)
        {
            req.flash('error',
            {
                msg: 'Password reset token is invalid or has expired.'
            });
            return res.redirect('/forgot');
        }
        res.render('reset',
        {
            title: 'Password Reset'
        });
    });
};

/**
 * POST /reset
 */
exports.resetPost = function(req, res, next)
{
    req.assert('password', 'Password must be at least 8 characters long').len(8);
    req.assert('confirm', 'Passwords must match').equals(req.body.password);

    var errors = req.validationErrors();

    if (errors)
    {
        req.flash('error', errors);
        return res.redirect('back');
    }

    async.waterfall([
        function(done)
        {
            User.findOne(
                {
                    passwordResetToken: req.params.token
                })
                .where('passwordResetExpires').gt(Date.now())
                .exec(function(err, user)
                {
                    if (!user)
                    {
                        req.flash('error',
                        {
                            msg: 'Password reset token is invalid or has expired.'
                        });
                        return res.redirect('back');
                    }
                    user.password = req.body.password;
                    user.passwordResetToken = undefined;
                    user.passwordResetExpires = undefined;
                    user.save(function(err)
                    {
                        req.logIn(user, function(err)
                        {
                            done(err, user);
                        });
                    });
                });
        },
        function(user, done)
        {
            Email.sendPasswordResetConfirmation(user.name, user.email, (error, data) => {
                req.flash('success',
                {
                    msg: 'Your password has been changed successfully.'
                });
                res.redirect('/account');
            });
        }
    ]);
};

/**
 * GET /confirm
 */
exports.confirmGet = function(req, res)
{
    User.findOne({
        emailConfirmToken: req.params.token
    })
    .where('emailConfirmExpires').gt(Date.now())
    .exec(function(err, user)
    {
        if (!user)
        {
            req.flash('error',
            {
                msg: 'Email confirmation token is invalid or has expired.'
            });
            return res.render('confirm',
            {
                title: 'Email Confirmation'
            });
        }
        user.emailConfirmed = true;
        user.emailConfirmToken = undefined;
        user.emailConfirmExpires = undefined;
        user.save(function(err)
        {
            if (err)
            {
                logger.error('Database error on user.save in GET /confirm/:token route', error);
            }
            req.flash('success',
            {
                msg: 'Email successfully confirmed. Thanks!'
            });
            res.render('confirm',
            {
                title: 'Email Confirmation'
            });
        });
    });
};

exports.confirmEmailOfUser = function(user) {
    confirmEmail(user);
};

function confirmEmail(user) {
    if (!user.emailConfirmed) {
        crypto.randomBytes(16, function(err, buf) {
            var token = buf.toString('hex');
            user.emailConfirmToken = token;
            user.emailConfirmExpires = Date.now() + 3600000*48; // expire in 48 hours
            user.emailConfirmationSent = true;
            user.save(function(err) {
                Email.sendConfirmationEmail(user.name, user.email, 'waitmate.io', token, (error, data) => { 
                    if (error) {
                        logger.error('Email error on sendConfirmationEmail in confirmEmail', error);            
                    }
                });
            });
        });
    }
}

function isUndf(obj) {
    return obj === undefined || obj === null;
}
