var mongoose = require('mongoose');
var User = require('./User');

var schemaOptions = {
    timestamps: true,
    toJSON:
    {
        virtuals: true
    }
};

var submissionSchema = new mongoose.Schema(
{
    user:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    restID: String,
    time: Number,
    isActive:
    {
        type: Boolean,
        default: true
    }
}, schemaOptions);

var Submission = mongoose.model('Submission', submissionSchema);

module.exports = Submission;
