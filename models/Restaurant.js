var mongoose = require('mongoose');

var schemaOptions = {
    timestamps: true,
    toJSON:
    {
        virtuals: true
    }
};

var restaurantSchema = new mongoose.Schema(
{
    id:
    {
        type: String,
        unique: true
    }
}, schemaOptions);

var Restaurant = mongoose.model('Restaurant', restaurantSchema);

module.exports = Restaurant;
