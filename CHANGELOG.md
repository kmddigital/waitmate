# Changelog

### 0.0.48
* Updated manifest.json

### 0.0.47
* Internal locations fix

### 0.0.46
* Internal locations fix

### 0.0.45
* Registration fix

### 0.0.44
* Internal locations fix

### 0.0.43
* Internal locations fix

### 0.0.42
* Restaurant page layout change
* Font Awesome v5

### 0.0.41
* Internal locations test
* Password security fix

### 0.0.40
* Activated internal locations

### 0.0.39
* Internal locations improvements
* User can now override submission

### 0.0.38
* Internal locations fix

### 0.0.37
* Preparation for internal locations

### 0.0.36
* Fixed lastpass autofill on admin user page

### 0.0.35
* Fixed API auth bug

### 0.0.34
* Added user and version data to Sentry
* Fixed Sentry bug

### 0.0.33
* Added Raven (Sentry)

### 0.0.32
* API now includes user data on error (except for auth)

### 0.0.31
* Typo fix

### 0.0.30
* API bug fix

### 0.0.29
* API bug testing

### 0.0.28
* Added social meta tags
* Changed homepage title

### 0.0.27
* Bumped version number
* Removed unneeded debugging

### 0.0.26
* Updated API Geolocation error to include location
* Added email message images
* Added email confirmation
* Added a reCaptcha on registration page

### 0.0.25
* Added check to make sure submitted wait times are between 0 and 180 minutes
* Fixed bug showing incorrect name and email on user admin page
* Fixed bug showing incorrect current user on another user's profile
* Added bottom margin to restaurant map
* Changed copyright message to be more condensed on mobile
* Fixed API error thrown if search found no results

### 0.0.24
* Quick fix to disable certain log messages

### 0.0.21
* Updated 404 page
* Removed unused dependencies

### 0.0.20
* Switched from Factual to Foursquare
* Improved error messages (work in progress)
* Improved restaurant page
  * Added map

### 0.0.15
* Added app page (work in progress, currently purposefully unreachable)
* Minor design overhaul
  * Changed Navbar to be white
  * Switched to native fonts
  * Changed previously grey items to white
  * Made WaitMate link in footer use ASAP font
* Added social links in footer
* Added email contact link in footer
* Added early release message
* Switched from alertify.js to Sweet Alert for error and info pop-ups

### 0.0.1 - 0.0.13
Built WaitMate and Admin Panel
