/* global $, swal */

var processingMessage = false; // eslint-disable-line

swal.setDefaults({
  confirmButtonColor: '#FF4A1C'
});

$(document).ready(function () {
  $('.back-to-top').click(function (event) {
    event.preventDefault();
    $('html, body').animate(
      {
        scrollTop: 0
      }, 300);
    return false;
  });

  if (getCookie('earlyReleaseMessage') !== 'true') {
    setTimeout(newUserMessage, 2000);
  }
});

function newUserMessage () {
  let text;
  let imageUrl = null;

  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    text = 'WaitMate is currently in an <b>early release</b> stage. This means certain features may be missing and will be updated in the future. Also, some waiting times may be unavailable and our mobile apps are not yet released, either. We still encourage you to use and contribute to WaitMate. We would appreciate your help by leaving feedback and inviting your friends. With each new user, more and more waiting times are submitted.';
  } else {
    text = 'WaitMate is currently in an <b>early release</b> stage. This means certain features are currently missing and will be added or updated in the future. Also, a significant amount of waiting times may be unavailable, as we have not grown our user base. Our mobile apps are not yet released, either. <br><br> Even so, we still encourage you to use and contribute to WaitMate as you see fit. We would appreciate your help by leaving feedback and inviting your friends. With each new user, more and more waiting times are submitted. <br><br> You Rock! <br> - The WaitMate Team';
    imageUrl = '/img/clock.png';
  }

  processingMessage = true;
  swal({
    title: 'Welcome to WaitMate!',
    text: text,
    imageUrl: imageUrl,
    allowOutsideClick: true,
    confirmButtonText: 'Okay',
    showLoaderOnConfirm: true,
    html: true
  });
  setCookie('earlyReleaseMessage', 'true', 3650);
}

function setCookie (cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = 'expires=' + d.toUTCString();
  document.cookie = cname + '=' + cvalue + '; ' + expires;
}

function getCookie (cname) {
  var name = cname + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}
