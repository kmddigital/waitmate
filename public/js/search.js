/* global $ */

var replaceAll = function (str, search, replacement) {
  return str.split(search).join(replacement);
};

window.onload = function () {
  document.getElementById('search').onsubmit = function () {
    if (document.getElementById('locbox').value === '') {
      document.getElementById('loc').className += ' has-error';
      $('#locbox').popover('show');
      return false;
    }
    if (document.getElementById('locbox').value === 'Current Location') {
      window.location = '/search?q=' + replaceAll(document.getElementById('searchbox').value, ' ', '+') + '&lat=' + getParameterByName('lat') + '&lng=' + getParameterByName('lng');
    } else {
      window.location = '/search?q=' + replaceAll(document.getElementById('searchbox').value, ' ', '+') + '&l=' + document.getElementById('locbox').value;
    }
    return false;
  };
};

function getParameterByName (name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
  var results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
