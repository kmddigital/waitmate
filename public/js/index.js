/* global $, google, swal, processingMessage */

var geocoder;
var currPosition;

var replaceAll = function (str, search, replacement) {
  return str.split(search).join(replacement);
};

window.onload = function () {
  geocoder = new google.maps.Geocoder();
  document.getElementById('search').onsubmit = function () {
    if (document.getElementById('locbox').value === '') {
      document.getElementById('loc').className += ' has-error';
      $('#locbox').popover('show');
      return false;
    }
    if (document.getElementById('locbox').value === 'Current Location') {
      window.location = '/search?q=' + replaceAll(document.getElementById('searchbox').value, ' ', '+') + '&lat=' + currPosition.coords.latitude + '&lng=' + currPosition.coords.longitude;
    } else {
      window.location = '/search?q=' + replaceAll(document.getElementById('searchbox').value, ' ', '+') + '&l=' + document.getElementById('locbox').value;
    }
    return false;
  };
  if (navigator.geolocation) {
    var optn = {
      enableHighAccuracy: true
    };
    navigator.geolocation.getCurrentPosition(updateLoc, locError, optn);
  } else {
    updateLocByIP();
  }
};

function nearby () { // eslint-disable-line
  if (document.getElementById('locbox').value === '') {
    document.getElementById('loc').className += ' has-error';
    $('#locbox').popover('show');
    return false;
  }
  if (document.getElementById('locbox').value === 'Current Location') {
    window.location = '/search' + '?lat=' + currPosition.coords.latitude + '&lng=' + currPosition.coords.longitude;
  } else {
    window.location = '/search?l=' + document.getElementById('locbox').value;
  }
}

function locError (error) {
  if (error.message.indexOf('Only secure origins are allowed') === 0) {
    if (getCookie('bdlrMessage') !== 'true' && !processingMessage) {
      swal('Oops...', 'Your browser forcible denied the request for Geolocation. Some browsers, such as Google Chrome, will deny requests not loaded over SSL.', 'error');
      setCookie('bdlrMessage', 'true', 365);
    }
  } else {
    switch (error.code) {
      case error.PERMISSION_DENIED:
        break;
      case error.POSITION_UNAVAILABLE:
        swal('Oops...', 'Your location information is unavailable', 'error');
        break;
      case error.TIMEOUT:
        swal('Oops...', 'The request to get your location timed out', 'error');
        break;
      case error.UNKNOWN_ERROR:
        swal('Oops...', 'An unknown error occurred when attempting to get your location', 'error');
        break;
    }
  }
  updateLocByIP();
}

function updateLoc (position) {
  currPosition = position;
  document.getElementById('locbox').value = 'Current Location';
    // codeLatLng(position.coords.latitude, position.coords.longitude);
}

function updateLocByIP () {
  $.getJSON('https://freegeoip.net/json/', function (response) {
    document.getElementById('locbox').value = response.city + ', ' + response.region_name;
  });
}

function codeLatLng (lat, lng) { // eslint-disable-line
  var latlng = new google.maps.LatLng(lat, lng);
  geocoder.geocode(
    {
      'latLng': latlng
    }, function (results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      if (results[1]) {
        document.getElementById('locbox').value = results[1].formatted_address;
      } else {
        swal('Oops...', 'No reverse geolocation results found.', 'error');
        updateLocByIP();
      }
    } else {
      swal('Oops...', 'Geocoder failed due to: ' + status + '.', 'error');
      updateLocByIP();
    }
  });
}

function setCookie (cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = 'expires=' + d.toUTCString();
  document.cookie = cname + '=' + cvalue + '; ' + expires;
}

function getCookie (cname) {
  var name = cname + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}
