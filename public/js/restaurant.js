/* global L, name, address, lat, lng */
var icon = L.AwesomeMarkers.icon({
  prefix: 'ion',
  icon: 'android-restaurant',
  markerColor: 'orange'
});
var map = L.map('map', { prefix: false }).setView([lat, lng], 13);
L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/light-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoia2VpdGhjciIsImEiOiJjaXp2aTc3d28wMHZwMnZxbGQxN3BwZ2NxIn0.2weeqx_U-CSPpD4lDBEbtg', {
  attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://mapbox.com">Mapbox</a>',
  maxZoom: 18,
  id: 'mapbox.streets',
  accessToken: 'pk.eyJ1Ijoia2VpdGhjciIsImEiOiJjaXp2aTc3d28wMHZwMnZxbGQxN3BwZ2NxIn0.2weeqx_U-CSPpD4lDBEbtg'
}).addTo(map);
map.attributionControl.setPrefix(false);
var marker = L.marker([lat, lng], {icon: icon}).addTo(map);
