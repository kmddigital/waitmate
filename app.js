var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var compression = require('compression');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var hbs = require('hbs');
var dotenv = require('dotenv');
var flash = require('express-flash');
var expressValidator = require('express-validator');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var moment = require('moment');
var winston = require('winston');
var Raven = require('raven');
var packagejson = require('./package.json');

// Load environment variables from .env file
dotenv.load();

process.title = 'WaitMate';

/* Database */
mongoose.connect(process.env.MONGODB);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('Successfully connected to Mongo database.');
});

Raven.config('https://9818c6f539504296b01d6dce83b3fca6:bdbf8db51cf5436ba324c90e454385f3@sentry.io/272138', { release: packagejson.version }).install();

// define app and locals
var app = module.exports = express();
app.locals.mongoose = mongoose;
app.locals.db = db;
app.locals.name = 'WaitMate';
app.locals.version = packagejson.version;
app.locals.foursquare = {id: process.env.FOURSQUARE_CLIENT_ID, secret: process.env.FOURSQUARE_CLIENT_SECRET};

// Disable certain Express settings
app.disable('x-powered-by');

app.use(Raven.requestHandler());

/* Logger */
var exceptionHandlers = process.env.NODE_ENV === 'development' ? null : [
  new winston.transports.File({
    name: 'exceptions',
    filename: 'logs/exceptions.log'
  })
];

var logger = new (winston.Logger)({
  levels: winston.config.syslog.levels,
  emitErrs: true,
  transports: [
    new (winston.transports.Console)({
      name: 'console.error',
      colorize: true,
      level: 'error',
      timestamp: function () {
        return Date.now();
      },
      formatter: function (options) {
        return moment(options.timestamp()).format('M[\\]D[\\]YYYY h:mm:ss a') + ' ' + options.level.toUpperCase() + ': ' + (undefined !== options.message ? options.message : '') +
                    (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '');
      }
    }),
    new (winston.transports.File)({
      name: 'file',
      filename: 'logs/server.log',
      level: 'debug'
    }),
    new (winston.transports.File)({
      name: 'human',
      filename: 'logs/human.log',
      level: 'debug',
      json: false,
      timestamp: function () {
        return Date.now();
      },
      formatter: function (options) {
        return moment(options.timestamp()).format('M[\\]D[\\]YYYY h:mm:ss a') + ' ' + options.level.toUpperCase() + ': ' + (undefined !== options.message ? options.message : '') +
                    (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) + '\n' : '');
      }
    })
  ],
  exceptionHandlers: exceptionHandlers
});
winston.addColors(winston.config.syslog.colors);

app.locals.logger = logger;


// Passport strategies
require('./config/passport');

// allow locals to be accessed in handlebars
hbs.localsAsTemplateData(app);

var routes = require('./routes/routes');
var users = require('./routes/users');
var admin = require('./routes/admin');
var restaurant = require('./routes/restaurant');
var search = require('./routes/search');
var internal = require('./routes/internal');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

hbs.registerPartials(path.join(__dirname, '/views/partials'));

hbs.registerHelper('getCurrentYear', function (a, b, options) {
  return new Date().getFullYear();
});

hbs.registerHelper('ifeq', function (a, b, options) {
  if (a === b) {
    return options.fn(this);
  }
  return options.inverse(this);
});

hbs.registerHelper('ifnoteq', function (a, b, options) {
  if (a !== b && !isUndf(a) && !isUndf(b)) {
    return options.fn(this);
  }
  return options.inverse(this);
});

hbs.registerHelper('toJSON', function (object) {
  return JSON.stringify(object, null, 4);
});

hbs.registerHelper('pluralize', function (count, string) {
  if (!isNaN(count) && parseInt(count) === 1)
    return string;
  else
  {
    if (string.slice(-1) === 'y')
      return string.substring(0, string.length - 1) + 'ies';
    else
      return string + 's';
  }
});

hbs.registerHelper('keepNumbers', function (string) {
  if (isUndf(string)) {
    return '';
  } else {
    return string.replace(/[^0-9]/g, '');
  }
});

hbs.registerHelper('fromNow', function (date) {
  return moment(date).fromNow();
});

hbs.registerHelper('fromNow', function (date, extra) {
  extra = extra === 'true';
  return moment(date).fromNow(extra);
});

hbs.registerHelper('formatDate', function (date) {
  return moment(date).format();
});

hbs.registerHelper('isOdd', function (num, options) {
  if (parseInt(num) % 2) {
    return options.fn(this);
  }
  return options.inverse(this);
});

hbs.registerHelper('toPrice', function (cat) {
  cat = parseInt(cat);
  if (cat === 0) {
    return 'Free :)';
  } else if (cat === 1) {
    return 'Inexpensive';
  } else if (cat === 2) {
    return 'Moderately Expensive';
  } else if (cat === 3) {
    return 'Expensive';
  } else if (cat === 4) {
    return 'Very Expensive';
  }
});

hbs.registerHelper('toStars', function (rating) {
  rating = Math.round(parseFloat(rating / 2) * 2) / 2;
  if (rating === 1.0) {
    return '<i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i>' +
            '<i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i>';
  } else if (rating === 1.5) {
    return '<span class="fa-stack"><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i></span><span class="fa-stack"><i class="fa fa-fw fa-2x fa-star-half fa-stack-1x"></i><i class="fa fa-fw fa-2x fa-star-o fa-stack-1x"></i></span>' +
            '<i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i><span class="fa-stack"><i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i></span><span class="fa-stack"><i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i></span>';
  } else if (rating === 2.0) {
    return '<i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i>' +
            '<i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i>';
  } else if (rating === 2.5) {
    return '<span class="fa-stack"><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i></span><span class="fa-stack"><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i></span><span class="fa-stack"><i class="fa fa-fw fa-2x fa-star-half fa-stack-1x"></i>' +
            '<i class="fa fa-fw fa-2x fa-star-o fa-stack-1x"></i></span><span class="fa-stack"><i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i></span><span class="fa-stack"><i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i></span>';
  } else if (rating === 3.0) {
    return '<i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i>' +
            '<i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i>';
  } else if (rating === 3.5) {
    return '<span class="fa-stack"><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i></span><span class="fa-stack"><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i></span><span class="fa-stack"><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i></span>' +
            '<span class="fa-stack"><i class="fa fa-fw fa-2x fa-star-half fa-stack-1x"></i><i class="fa fa-fw fa-2x fa-star-o fa-stack-1x"></i></span><span class="fa-stack"><i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i></span>';
  } else if (rating === 4.0) {
    return '<i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i>' +
            '<i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star-o fa-fw fa-2x" aria-hidden="true"></i>';
  } else if (rating === 4.5) {
    return '<span class="fa-stack"><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i></span><span class="fa-stack"><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i></span><span class="fa-stack"><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i></span>' +
            '<span class="fa-stack"><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i></span><span class="fa-stack"><i class="fa fa-fw fa-2x fa-star-half fa-stack-1x"></i><i class="fa fa-fw fa-2x fa-star-o fa-stack-1x"></i></span>';
  } else if (rating === 5.0) {
    return '<i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i>' +
            '<i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i><i class="fa fa-star fa-fw fa-2x" aria-hidden="true"></i>';
  }
});

hbs.registerHelper('isOpen', function (day, hour, hours, options) {
  let hoursArray;
  switch (day) {
    case 0:
      hoursArray = hours.sunday;
      break;
    case 1:
      hoursArray = hours.monday;
      break;
    case 2:
      hoursArray = hours.tuesday;
      break;
    case 3:
      hoursArray = hours.wednesday;
      break;
    case 4:
      hoursArray = hours.thursday;
      break;
    case 5:
      hoursArray = hours.friday;
      break;
    case 6:
      hoursArray = hours.saturday;
      break;
  }
  if (isUndf(hoursArray)) {
    return options.inverse(this);
  }
  for (let i = 0; i < hoursArray.length; i++) {
    if (hour > parseInt(hoursArray[i][0]) && hour < parseInt(hoursArray[i][1])) {
      return options.fn(this);
    }
  }
  return options.inverse(this);
});

hbs.registerHelper('ifAllNotEq', function (args, check, options) {
  for (var i = 0; i < args.length; i++) {
    if (arguments[i] !== check && !isUndf(args[i]) && !isUndf(check)) {
      return options.fn(this);
    }
  }
  return options.inverse(this);
});

hbs.registerHelper('formatDateAndTime', function (date, options) {
  return moment(new Date(date)).format('MMMM Do YYYY, h:mm:ss a');
});

hbs.registerHelper('levelToIcon', function (level, options) {
  var icon = 'question';
  switch (level) {
    case 'emerg':
      icon = 'exclamation-triangle';
      break;
    case 'alert':
      icon = 'bell';
      break;
    case 'crit':
      icon = 'exclamation-circle';
      break;
    case 'error':
      icon = 'exclamation';
      break;
    case 'warning':
      icon = 'flag';
      break;
    case 'notice':
      icon = 'info-circle';
      break;
    case 'info':
      icon = 'info';
      break;
    case 'debug':
      icon = 'bug';
      break;
    default:
      break;
  }
  return 'fa-' + icon;
});

hbs.registerHelper('formatAddress', function (address, options) {
  if (address === undefined) {
    return '';
  }
  let str = '';
  for (let i = 0; i < address.length; i++) {
    str += address[i];
    if (i !== address.length - 1) {
      str += ', ';
    }
  }
  return str;
});

hbs.registerHelper('arrayNotEmpty', function (arr, options) {
  if (arr && arr.length > 0) {
    return options.fn(this);
  }
  return options.inverse(this);
});

hbs.registerHelper('formatValue', function (value, options) {
  if (value.toLowerCase().includes('yes')) {
    return '<i class="fa fa-check fa-fw" style="color: #00FF00;" title="' + value + '"></i>';
  } else if (value.toLowerCase().includes('no')) {
    return '<i class="fa fa-times fa-fw" style="color: #FF0000;" title="' + value + '"></i>';
  } else {
    return value;
  }
});

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(compression());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(expressValidator());
app.use(methodOverride('_method'));
app.use(session({
  secret: process.env.SESSION_SECRET,
  resave: true,
  saveUninitialized: true,
  maxAge: new Date(Date.now() + 3600000),
  store: new MongoStore({
    mongooseConnection: mongoose.connection
  })
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.removeHeader('x-powered-by');
  res.locals.user = req.user;
  if (req.user)
  {
    Raven.setContext({
      user: {
        email: req.user.email,
        name: req.user.name
      }
    });
  }
  res.locals.url = req.protocol + '://' + req.get('host') + req.originalUrl;
  if (!req.get('host').includes('waitmate') && !req.get('host').includes('localhost') && !req.get('host').includes('45.55.169.206') &&
        !app.get('env') === 'development') {
    res.render('error', {
      message: 'Slow down there cowboy, you\'re way off the reservation',
      info: 'You tried to access this site from a domain or IP that doesn\'t belong to WaitMate. <a href="https://waitmate.io/">Go Home</a>.',
      error:
            {},
      title: 'Error'
    });
  } else {
    next();
  }
});

// pass request to correct handler
app.use('/', routes);
app.use('/users', users);
app.use('/admin', admin);
app.use('/restaurant', restaurant);
app.use('/search', search);
app.use('/internal', internal);

app.use(Raven.errorHandler());

// no handler found, catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    if (err.status === 404) {
      res.status(404);
      res.render('404', {
        title: '404'
      });
    } else {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err,
        sentry: res.sentry,
        title: 'Error'
      });
    }
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  if (err.status !== 404) {
    logger.error('Renderer error in production handler', {
      url: res.locals.url,
      error: err
    });
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {},
      sentry: res.sentry,
      title: 'Error'
    });
  } else {
    res.status(404);
    res.render('404', {
      title: '404'
    });
  }
});

function isUndf (obj) {
  return obj === undefined || obj === null;
}
