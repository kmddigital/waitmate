<img src="https://waitmate.io/img/logo.svg" alt="WaitMate Logo"/>

## Backend And Website
### To install:  
__Note__: Before installing, be sure to setup [SSH access](https://gitlab.com/help/ssh/README.md).
```bash
git clone git@gitlab.com:kmddigital/waitmate.git
npm install
```

### To deploy:
```bash
git remote add live ssh://waitmate@45.55.169.206/home/waitmate/repo/site.git
git add -A
git commit -m "Some Fix"
# Push to development repository
git push -u origin master
# Push to live server
git push -u live master
```

### To run:
```bash
npm start
```

Naviagte to [localhost:3000](localhost:3000).
