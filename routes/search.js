var express = require('express');
var router = express.Router();
var app = require('../app');
var Submission = require('../models/Submission');
var moment = require('moment');
var geocoder = require('geocoder');
var request = require('request');

const replaceAll = function (str, search, replacement) {
  return str.split(search).join(replacement);
};

var logger = app.locals.logger;

var foursquareRequest = request.defaults({
  qs: {
    'client_id': app.locals.foursquare.id,
    'client_secret': app.locals.foursquare.secret,
    'v': '20170725'
  }
});

/* GET search page. */
router.get('/', function (req, res, next) {
  if (isUndf(req.query.q)) {
    req.query.q = '';
  }
  if (isUndf(req.query.l)) {
    req.query.l = '';
  }
  if (isUndf(req.query.p) || isNaN(req.query.p) || parseInt(req.query.p) < 1) {
    req.query.p = '1';
  }
  if (isUndf(req.query.lat) || isUndf(req.query.lng)) {
    req.query.lat = '';
    req.query.lng = '';
  }
  var query = replaceAll(req.query.q, '+', ' ');
  var loc = req.query.l;
  var lng = req.query.lng;
  var lat = req.query.lat;
  var pageNumber = parseInt(req.query.p);
  if (lat !== '' && lng !== '') {
    loadSearch(req, res, next, query, 'Current Location', lat, lng, pageNumber, req.originalUrl);
  } else {
    geocoder.geocode(loc, function (err, data) {
      if (err) {
        logger.error('search.js?v=15082017:41 Geocoding error', err);
        res.render('search', {
          title: 'Search',
          description: 'Find results for restaurant waiting times near you.',
          query: query,
          loc: loc,
          error: ['We had a problem finding your location. ' + err],
          customjs: 'search.js?v=15082017'
        });
      } else if (data.results.length === 0) {
        logger.info('search.js?v=15082017:54 Geocoding: No location found', loc);
        res.render('search', {
          title: 'Search',
          description: 'Find results for restaurant waiting times near you.',
          query: query,
          loc: loc,
          error: ['We had a problem finding your location. Double-check it and try again.'],
          customjs: 'search.js?v=15082017'
        });
      } else {
        loadSearch(req, res, next, query, loc, data.results[0].geometry.location.lat, data.results[0].geometry.location.lng, pageNumber, req.originalUrl);
      }
    });
  }
});

function loadSearch (req, res, next, query, loc, lat, lng, pageNumber, originalUrl) {
  let url = 'https://api.foursquare.com/v2/venues/explore?ll=' + lat + ',' + lng + '&limit=50&sortByDistance=1&offset=' + ((pageNumber - 1) * 50);
  url += query !== '' ? '&query=' + query : '&query=coffee,drinks,dessert,bar&categoryId=4d4b7105d754a06374d81259,4bf58dd8d48988d116941735';
  let nextPageNumber = pageNumber;
  let previousPageNumber = pageNumber === 1 ? pageNumber : pageNumber - 1;
  foursquareRequest(url, function (error, response, body) {
    if (error) {
      logger.error('Request error on foursquareRequest in GET /search route', error);
    }
    body = JSON.parse(body);
    if (body.meta.errorType) {
      logger.error('Foursquare error in GET /search route', body.meta);
      return res.render('search', {
        title: 'Search',
        description: 'Find results for restaurant waiting times near you.',
        query: query,
        loc: loc,
        error: ['Search Error: ' + body.meta.errorDetail],
        customjs: 'search.js?v=15082017'
      });
    }
    let result = body.response;
    result.venues = [];
    if (!result.groups || result.groups.length === 0) {
      logger.error('Foursquare returned no results in GET /search route', body);
      return res.render('search', {
        title: 'Search',
        description: 'Find results for restaurant waiting times near you.',
        query: query,
        loc: loc,
        error: ['Search Error: Foursquare returned no results'],
        customjs: 'search.js?v=15082017'
      });
    }
    for (let i in result.groups) {
      for (let k in result.groups[i].items) {
        result.venues.push(result.groups[i].items[k].venue);
      }
    }
    if (parseInt(result.totalResults) > pageNumber * 50) {
      nextPageNumber = nextPageNumber + 1;
    }
    var ids = [];
    for (var i = 0; i < result.venues.length; i++) {
      ids.push(result.venues[i].id);
    }
    Submission.find({
      restID: {
        $in: ids
      },
      createdAt: {
        $gt: new Date(Date.now() - 5 * 60 * 1000)
      },
      isActive: true
    }).exec(function (error, results) {
      if (error) { logger.error('search.js?v=15082017:120 Database error', error); }
      var averages = new Map();
      var counts = new Map();
      var index;
      for (index in result.venues) {
        averages.set(result.venues[index].id, 0);
        counts.set(result.venues[index].id, 0);
      }
      for (i = 0; i < results.length; i++) {
        averages.set(results[i].restID, averages.get(results[i].restID) + parseInt(results[i].time));
        counts.set(results[i].restID, counts.get(results[i].restID) + 1);
      }
      var keys = averages.keys();
      var key;
      for (key of keys) {
        var count = counts.get(key);
        if (count === 0) {
          averages.set(key, 'Unknown');
        } else {
          averages.set(key, precisionRound(averages.get(key) / count, 0));
        }
      }
      for (i = 0; i < result.venues.length; i++) {
        result.venues[i].average = averages.get(result.venues[i].id);
      }
      res.render('search', {
        title: 'Search',
        description: 'Find results for restaurant waiting times near you.',
        query: query,
        loc: loc,
        lat: lat,
        lng: lng,
        results: result,
        orginalUrl: originalUrl,
        pageNumber: pageNumber,
        nextPageNumber: nextPageNumber,
        previousPageNumber: previousPageNumber,
        customjs: 'search.js?v=15082017',
        day: moment().day(),
        hour: moment().hour()
      });
    });
  });
}

function isUndf (obj) {
  return obj === undefined || obj === null;
}

function precisionRound(number, precision) {
  var factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
}

module.exports = router;
