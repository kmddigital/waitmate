var express = require('express');
var router = express.Router();
var app = require('../app');
var Submission = require('../models/Submission');
var User = require('../models/User');
var moment = require('moment');
var request = require('request');

var logger = app.locals.logger;

var foursquareRequest = request.defaults({
  qs: {
    'client_id': app.locals.foursquare.id,
    'client_secret': app.locals.foursquare.secret,
    'v': '20170725'
  }
});

/* GET restaurant page. */
router.get('/:id', function (req, res, next) {
  foursquareRequest('https://api.foursquare.com/v2/venues/' + req.params.id, function (error, response, body) {
    if (error) {
      logger.error('Request error on foursquareRequest in GET /restaurant/:id route', error);
    }
    body = JSON.parse(body);
    if (body.meta.errorDetail && body.meta.errorDetail.includes('is invalid for venue id')) {
      next();
    } else {
      loadRest(req, res, next, body.meta, body.response.venue);
    }
  });
});

function loadRest (req, res, next, meta, result) {
  result.serves = [];
  for (let group in result.attributes.groups) {
    group = result.attributes.groups[group];
    switch (group.type) {
      case 'reservations':
        result.reservations = group.items[0].displayValue;
        break;
      case 'payments':
        console.log('payments');
        result.creditCards = group.items[0].displayValue;
        break;
      case 'outdoorSeating':
        result.outdoorSeating = group.items[0].displayValue;
        break;
      case 'wifi':
        result.wifi = group.items[0].displayValue;
        break;
      case 'serves':
        for (let item in group.items) {
          item = group.items[item];
          result.serves.push(item.displayName);
        }
        break;
      case 'drinks':
        for (let item in group.items) {
          item = group.items[item];
          result.serves.push(item.displayName);
        }
        break;
      case 'diningOptions':
        for (let item in group.items) {
          item = group.items[item];
          switch (item.displayName) {
            case 'Delivery':
              result.delivery = item.displayValue;
              break;
          }
        }
        break;
    }
  }
  Submission.find({
    restID: req.params.id,
    createdAt: {
      $gt: new Date(Date.now() - 5 * 60 * 1000)
    },
    isActive: true
  }).exec(function (error, results) {
    if (error) {
      logger.error('restaurant.js?v=15082017:39 Database error', error);
    }
    var average;
    if (results.length !== 0) {
      average = 0;
      for (let i = 0; i < results.length; i++) {
        average += parseInt(results[i].time);
      }
      average = precisionRound(average / results.length, 0);
    } else {
      average = 'Unknown';
    }
    if (meta.error) {
      logger.error('restaurant.js?v=15082017:56 Google Places error', meta.error);
      res.render('restaurant', {
        title: 'Restaurant',
        description: 'Get restaurant waiting times submitted by users such as yourself. Spend less time waiting and more time eating.',
        waitTime: average,
        id: req.params.id,
        error: ['Failed to load data for this restaurant. Wait times can still be submitted and viewed but other data about this restaurant is currently unavailable.', meta.errorDetail]
      });
    } else {
      if (result === undefined) {
        next();
      } else {
        let socialShareText = average === 'Unknown' ? 'Stay up to date on ' + result.name + ' waiting times with ' + app.locals.name + '.' : 'There is currently a ' + average +
                ' ' + pluralize(average, 'minute') + ' wait at ' + result.name + ' via ' + app.locals.name + '.';
        let socialShareTextTwitter = average === 'Unknown' ? 'Stay up to date on ' + result.name + ' waiting times with ' + app.locals.name + '.' : 'There is currently a ' + average +
                ' ' + pluralize(average, 'minute') + ' wait at ' + result.name + ' via ' + app.locals.name + '.';
        res.render('restaurant', {
          title: result.name,
          description: 'Get the current waiting time for ' + result.name + ' and others submitted by users such as yourself.',
          data: result,
          results: results,
          waitTime: average,
          socialShareText: encodeURIComponent(socialShareText),
          socialShareTextTwitter: encodeURIComponent(socialShareTextTwitter),
          socialShareUrl: encodeURIComponent(req.protocol + '://' + req.get('host') + req.originalUrl),
          foursquareID: app.locals.foursquare.id,
          customjs: 'restaurant.js?v=18012018',
          day: moment().day(),
          hour: moment().hour()
        });
      }
    }
  });
}

/* POST restaurant page. */
router.post('/:id', function (req, res, next) {
  if (!req.user) {
    res.redirect('/login?return=' + req.protocol + '://' + req.get('host') + req.originalUrl);
  }
  req.assert('waitTime', 'Wait time cannot be blank.').notEmpty();
  req.assert('waitTime', 'Wait time must be a number.').isInt();
  req.assert('waitTime', 'Wait time must be between 0 and 180 minutes.').isInt({ min: 0, max: 180 });

  var errors = req.validationErrors();

  if (errors) {
    req.flash('error', errors);
    return res.redirect(req.protocol + '://' + req.get('host') + req.originalUrl);
  }

  Submission.findOne({
    restID: req.params.id,
    user: req.user.id,
    createdAt: {
      $gt: new Date(Date.now() - 5 * 60 * 1000)
    },
    isActive: true
  }).exec(function (error, sub) {
    if (error) { logger.error('restaurant.js?v=15082017:129 Database error', error); }
    if (isUndf(sub) || new Date(Date.now() - 300000).getTime() >= sub.createdAt.getTime()) {
      let submission = new Submission({
        user: req.user.id,
        restID: req.params.id,
        time: parseInt(req.body.waitTime)
      });
      submission.save(function (err) {
        if (err) {
          logger.error('restaurant.js?v=15082017:142 Database error', error);
          req.flash('error', {
            msg: 'An error has occured. Please try again later.'
          });
        } else {
          req.flash('success', {
            msg: 'Wait time submitted. Thanks!'
          });
        }
        User.findById(req.user.id, function (err, user) {
          if (err) {
            logger.error('Database error on user.findById in POST /restaurant/:id route', err);
          }
          user.lastSubmission = new Date();
          user.numSubmissions = user.numSubmissions + 1;
          user.save(function (err) {
            if (err) {
              console.error(err);
            }
            res.redirect(req.protocol + '://' + req.get('host') + req.originalUrl);
          });
        });
      });
    } else {
      sub.isActive = false;
      sub.save(function (err) {
        if (err) {
          logger.error('Database error on sub.save in POST /restaurant/:id route', err);
        }
        let submission = new Submission({
          user: req.user.id,
          restID: req.params.id,
          time: parseInt(req.body.waitTime)
        });
        submission.save(function (err) {
          if (err) {
            logger.error('restaurant.js?v=15082017:142 Database error', error);
            req.flash('error', {
              msg: 'An error has occured. Please try again later.'
            });
          } else {
            req.flash('success', {
              msg: 'Wait time submitted. Thanks!'
            });
          }
          User.findById(req.user.id, function (err, user) {
            if (err) {
              logger.error('Database error on user.findById in POST /restaurant/:id route', err);
            }
            user.lastSubmission = new Date();
            user.numSubmissions = user.numSubmissions + 1;
            user.save(function (err) {
              if (err) {
                console.error(err);
              }
              res.redirect(req.protocol + '://' + req.get('host') + req.originalUrl);
            });
          });
        });
      });
    }
  });
});

function isUndf (obj) {
  return obj === undefined || obj === null;
}

function pluralize (count, string) {
  return !isNaN(count) && parseInt(count) === 1 ? string : string + 's';
}

function precisionRound(number, precision) {
  var factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
}

module.exports = router;
