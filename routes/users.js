var express = require('express');
var User = require('../models/User');
var router = express.Router();

/* GET users listing. */
router.get('/:user', function (req, res, next) {
  if (!req.params.user.match(/^[0-9a-fA-F]{24}$/)) {
    return next();
  }
  User.findById(req.params.user, function (err, user) {
    if (err) {
      res.render('error', {
        message: err.message,
        error: err,
        title: 'Error'
      });
    } else if (isUndf(user)) {
      next();
    } else {
      res.render('user', {
        title: user.name,
        profile: user
      });
    }
  });
});

function isUndf (obj) {
  return obj === undefined || obj === null;
}

module.exports = router;
