var express = require('express');
var router = express.Router();
var app = require('../app');
var Submission = require('../models/Submission');
var User = require('../models/User');
var moment = require('moment');
var geocoder = require('geocoder');
var jwt = require('jsonwebtoken');
var passport = require('passport');
var request = require('request');
var userController = require('../controllers/user');

const replaceAll = function (str, search, replacement) {
  return str.split(search).join(replacement);
};

var logger = app.locals.logger;

var foursquareRequest = request.defaults({
  qs: {
    'client_id': app.locals.foursquare.id,
    'client_secret': app.locals.foursquare.secret,
    'v': '20170725'
  }
});

/* GET restaurant */
router.get('/api/restaurant/:id', verifyToken, function (req, res, next) {
  if (req.params.id == 'cadininghall' && compareVersion(req.query.version, "0.1.9"))
  {
    Submission.find({
      restID: req.params.id,
      createdAt: {
        $gt: new Date(Date.now() - 5 * 60 * 1000)
      }
    }).exec(function (error, results) {
      if (error) {
        return res.send({
          error: error,
          user: req.user
        });
      }
      let data = {
        id: 'cadininghall',
        name: 'CA Dining Hall',
        location: {
          formattedAddress: [
            "4300 Cherry Bottom Road",
            "Gahanna, OH 43230",
            "United States"
          ]
        },
        hasMenu: true,
        menu: {
          type: "Menu",
          label: "Menu",
          anchor: "View Menu",
          url: "http://www.sagedining.com/menus/columbusacademy",
          mobileUrl: "http://www.sagedining.com/menus/columbusacademy"
        },
        contact: {},
        categories: {},
        allowMenuUrlEdit: true,
        internal: true
      };
      var average;
      if (results.length !== 0) {
        average = 0;
        for (let i = 0; i < results.length; i++) {
          average += parseInt(results[i].time);
        }
        average = precisionRound(average / results.length, 0);
      } else {
        average = 'Unknown';
      }
      data.results = results;
      data.average = average;
      data.serves = [];
      res.send({
        data: data,
        user: req.user
      });
    });
  }
  else
  {
    foursquareRequest('https://api.foursquare.com/v2/venues/' + req.params.id, function (error, response, body) {
      if (error) {
        logger.error('Request error on foursquareRequest in GET /internal/api/restaurant/:id route', error);
      }
      body = JSON.parse(body);
      if (body.meta.errorDetail && body.meta.errorDetail.includes('is invalid for venue id')) {
        next();
      } else {
        Submission.find({
          restID: req.params.id,
          createdAt: {
            $gt: new Date(Date.now() - 5 * 60 * 1000)
          },
          isActive: true
        }).exec(function (error, results) {
          if (error) {
            return res.send({
              error: error,
              user: req.user
            });
          }
          var average;
          if (results.length !== 0) {
            average = 0;
            for (let i = 0; i < results.length; i++) {
              average += parseInt(results[i].time);
            }
            average = average / results.length;
          } else {
            average = 'Unknown';
          }
          let data = body.response.venue;
          data.results = results;
          data.average = average;
          data.serves = [];
          for (let group in data.attributes.groups) {
            group = data.attributes.groups[group];
            switch (group.type) {
              case 'reservations':
                data.reservations = group.items[0].displayValue;
                break;
              case 'payments':
                data.creditCards = group.items[0].displayValue;
                break;
              case 'outdoorSeating':
                data.outdoorSeating = group.items[0].displayValue;
                break;
              case 'wifi':
                data.wifi = group.items[0].displayValue;
                break;
              case 'serves':
                for (let item in group.items) {
                  item = group.items[item];
                  data.serves.push(item.displayName);
                }
                break;
              case 'drinks':
                for (let item in group.items) {
                  item = group.items[item];
                  data.serves.push(item.displayName);
                }
                break;
              case 'diningOptions':
                for (let item in group.items) {
                  item = group.items[item];
                  switch (item.displayName) {
                    case 'Delivery':
                      data.delivery = item.displayValue;
                      break;
                  }
                }
                break;
            }
          }
          res.send({
            data: data,
            user: req.user
          });
        });
      }
    });
  }
});

/* POST restaurant. */
router.post('/api/restaurant/:id', verifyToken, function (req, res, next) {
  if (req.query.waitTime) {
    req.body.waitTime = req.query.waitTime;
  }
  req.assert('waitTime', 'Wait time cannot be blank.').notEmpty();
  req.assert('waitTime', 'Wait time must be a number.').isInt();
  req.assert('waitTime', 'Wait time must be between 0 and 180 minutes.').isInt({ min: 0, max: 180 });

  var errors = req.validationErrors();

  if (errors) {
    return res.send({
      error: {
        msg: 'See errors',
        status: 400
      },
      errors: errors,
      success: false,
      user: req.user
    });
  }

  Submission.findOne({
    restID: req.params.id,
    user: req.user.id,
    createdAt: {
      $gt: new Date(Date.now() - 5 * 60 * 1000)
    },
    isActive: true
  }).exec(function (error, sub) {
    if (error) {
      logger.error('Database error on Submission.findOne in POST /internal/api/restaurant/:id route', error);
    }
    if (isUndf(sub) || new Date(Date.now() - 300000).getTime() >= sub.createdAt.getTime()) {
      let submission = new Submission({
        user: req.user.id,
        restID: req.params.id,
        time: parseInt(req.body.waitTime)
      });
      submission.save(function (err) {
        if (err) {
          logger.error('Database error on submission.save in POST /internal/api/restaurant/:id route', err);
        }
        User.findById(req.user.id, function (err, user) {
          if (err) {
            logger.error('Database error on User.findById in POST /internal/api/restaurant/:id route', err);
          }
          user.lastSubmission = new Date();
          user.numSubmissions = user.numSubmissions + 1;
          user.save(function (err) {
            if (err) {
              return res.send({
                error: err,
                user: req.user
              });
            }
            res.send({
              success: true,
              user: req.user
            });
          });
        });
      });
    } else {
      sub.isActive = false;
      sub.save(function (err) {
        if (err) {
          logger.error('Database error on sub.save in POST /restaurant/:id route', err);
        }
        let submission = new Submission({
          user: req.user.id,
          restID: req.params.id,
          time: parseInt(req.body.waitTime)
        });
        submission.save(function (err) {
          if (err) {
            logger.error('restaurant.js?v=15082017:142 Database error', error);
            req.flash('error', {
              msg: 'An error has occured. Please try again later.'
            });
          } else {
            req.flash('success', {
              msg: 'Wait time submitted. Thanks!'
            });
          }
          User.findById(req.user.id, function (err, user) {
            if (err) {
              logger.error('Database error on user.findById in POST /restaurant/:id route', err);
            }
            user.lastSubmission = new Date();
            user.numSubmissions = user.numSubmissions + 1;
            user.save(function (err) {
              if (err) {
                console.error(err);
              }
              res.send({
                success: true,
                user: req.user
              });
            });
          });
        });
      });
    }
  });
});

/* GET search page. */
router.get('/api/search', verifyToken, function (req, res, next) {
  if (isUndf(req.query.q)) {
    req.query.q = '';
  }
  if (isUndf(req.query.p) || isNaN(req.query.p) || parseInt(req.query.p) < 1) {
    req.query.p = '1';
  }
  if ((isUndf(req.query.lat) || isUndf(req.query.lng)) && isUndf(req.query.loc)) {
    res.send({
      error: {
        msg: 'No lat, lng or loc provided.',
        status: 400
      },
      user: req.user
    });
  } else {
    var query = replaceAll(req.query.q, '+', ' ');
    var loc = req.query.loc || '';
    var lng = req.query.lng || '';
    var lat = req.query.lat || '';
    var page = parseInt(req.query.p);
    var offset = req.query.offset || (page - 1) * 50;
    if (lat !== '' && lng !== '') {
      loadSearch(req, res, next, query, lat, lng, page, offset);
    } else {
      geocoder.geocode(loc, function (err, data) {
        if (err) {
          res.send({
            error: err,
            user: req.user
          });
        } else if (data.results.length === 0) {
          res.send({
            error: {
              msg: 'No geocoding results found for ' + loc + ' .',
              status: 404
            },
            user: req.user
          });
        } else { loadSearch(req, res, next, query, data.results[0].geometry.location.lat, data.results[0].geometry.location.lng, page, offset); }
      });
    }
  }
});

function loadSearch (req, res, next, query, lat, lng, pageNumber, offset) {
  let url = 'https://api.foursquare.com/v2/venues/explore?ll=' + lat + ',' + lng + '&limit=50&sortByDistance=1&offset=' + ((pageNumber - 1) * 50);
  url += query !== '' ? '&query=' + query : '&query=coffee,drinks,dessert,bar&categoryId=4d4b7105d754a06374d81259,4bf58dd8d48988d116941735';
  let nextPageNumber = pageNumber;
  foursquareRequest(url, function (error, response, body) {
    if (error) {
      logger.error('Request error on foursquareRequest in GET /internal/api/search route', error);
    }
    body = JSON.parse(body);
    let result = body.response;
    result.venues = [];
    if (body.meta.error) {
      logger.error('Foursquare error in GET /internal/api/search route', body.meta.error);
      return res.send({
        error: body.meta.error,
        user: req.user
      });
    }
    if (!result.groups)
    {
      return res.send({
        error: { msg: 'No results.' },
        user: req.user
      });
    }
    for (let i in result.groups) {
      for (let k in result.groups[i].items) {
        result.venues.push(result.groups[i].items[k].venue);
      }
    }
    if (!isNaN(lat) && !isNaN(lng) && 40.0400000 <= parseFloat(lat) && parseFloat(lat) <= 40.0599999 && -82.8899999 <= parseFloat(lng) && parseFloat(lng) <= -82.8600000  && compareVersion(req.query.version, "0.1.9"))
    {
      result.venues.unshift({
        id: 'cadininghall',
        name: 'CA Dining Hall',
        location: {
          formattedAddress: [
            "4300 Cherry Bottom Road",
            "Gahanna, OH 43230",
            "United States"
          ]
        },
        hasMenu: true,
        menu: {
          type: "Menu",
          label: "Menu",
          anchor: "View Menu",
          url: "http://www.sagedining.com/menus/columbusacademy",
          mobileUrl: "http://www.sagedining.com/menus/columbusacademy"
        },
        contact: {},
        categories: {},
        allowMenuUrlEdit: true,
        internal: true
      });
    }
    if (parseInt(result.totalResults) > pageNumber * 50) {
      nextPageNumber = nextPageNumber + 1;
    }
    var ids = [];
    for (var i = 0; i < result.venues.length; i++) {
      ids.push(result.venues[i].id);
    }
    Submission.find({
      restID: {
        $in: ids
      },
      createdAt: {
        $gt: new Date(Date.now() - 5 * 60 * 1000)
      },
      isActive: true
    }).exec(function (error, results) {
      if (error) { logger.error('Database error on submission.find in GET /internal/api/search route', error); }
      var averages = new Map();
      var counts = new Map();
      var index;
      for (index in result.venues) {
        averages.set(result.venues[index].id, 0);
        counts.set(result.venues[index].id, 0);
      }
      for (i = 0; i < results.length; i++) {
        averages.set(results[i].restID, averages.get(results[i].restID) + parseInt(results[i].time));
        counts.set(results[i].restID, counts.get(results[i].restID) + 1);
      }
      var keys = averages.keys();
      var key;
      for (key of keys) {
        var count = counts.get(key);
        if (count === 0) {
          averages.set(key, 'Unknown');
        } else {
          averages.set(key, precisionRound(averages.get(key) / count, 0));
        }
      }
      for (i = 0; i < result.venues.length; i++) {
        result.venues[i].average = averages.get(result.venues[i].id);
      }
      res.send({
        results: result,
        user: req.user,
        pageNumber: pageNumber,
        nextPageNumber: nextPageNumber
      });
    });
  });
}

router.post('/api/auth', function (req, res, next) {
  if (req.query.email) { req.body.email = req.query.email; }
  if (req.query.password) {
    req.body.password = req.query.password;
  }
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('email', 'Email cannot be blank').notEmpty();
  req.assert('password', 'Password cannot be blank').notEmpty();
  req.sanitize('email').normalizeEmail({
    remove_dots: false
  });

  var errors = req.validationErrors();

  if (errors) {
    return res.send({
      error: {
        msg: 'See errors',
        status: 400
      },
      errors: errors
    });
  }

  passport.authenticate('local', {
    session: false
  }, function (err, user, info) {
    if (!user || err) {
      res.send({
        error: {
          msg: info,
          status: 400
        }
      });
    } else {
      jwt.sign(user.id, process.env.JWT_SECRET, {}, function (error, token) {
        if (error) {
          logger.error('JWT error on jwt.sign in POST /internal/api/auth route', error);
        }
        res.send({
          token: token,
          user: user
        });
      });
    }
  })(req, res, next);
});

router.post('/api/register', function (req, res, next) {
  if (req.query.email) {
    req.body.email = req.query.email;
  }
  if (req.query.password) {
    req.body.password = req.query.password;
  }
  if (req.query.name) {
    req.body.name = req.query.name;
  }

  req.assert('name', 'Name cannot be blank').notEmpty();
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('email', 'Email cannot be blank').notEmpty();
  req.assert('password', 'Password must be at least 4 characters long').len(4);
  req.sanitize('email').normalizeEmail({
    remove_dots: false
  });

  var errors = req.validationErrors();

  if (errors) {
    return res.send({
      error: {
        msg: 'See errors',
        status: 400
      },
      errors: errors
    });
  }

  User.findOne({
    email: req.body.email
  }, function (err, user) {
    if (err) {
      logger.error('Database error on User.findOne in POST /internal/api/register route', err);
    }
    if (user) {
      return res.send({
        error: {
          msg: 'The email address you have entered is already associated with another account.',
          status: 400
        },
        errors: errors
      });
    }
    user = new User({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      emailConfirmationSent: true
    });
    user.save(function (err) {
      if (err) {
        logger.error('Database error on user.save in POST /internal/api/register route', err);
      }
      passport.authenticate('local', {
        session: false
      }, function (err, user, info) {
        if (err) {
          logger.error('Passport error on passport.authenticate in POST /internal/api/register route', err);
        }
        if (!user) {
          res.send({
            error: {
              msg: info,
              status: 400
            }
          });
        } else {
          userController.confirmEmailOfUser(user);
          jwt.sign(user.id, process.env.JWT_SECRET, {}, function (error, token) {
            if (error) {
              logger.error('JWT error on jwt.sign in POST /internal/api/register route', error);
            }
            res.send({
              token: token,
              user: user
            });
          });
        }
      })(req, res, next);
    });
  });
});

function isUndf (obj) {
  return obj === undefined || obj === null;
}

function verifyToken (req, res, next) {
  let token = req.body.token || req.query.token;
  if (!token) {
    return res.send({
      error: {
        status: 401,
        msg: 'Missing auth token'
      }
    });
  }
  jwt.verify(token, process.env.JWT_SECRET, {}, function (error, decoded) {
    if (error) {
      return res.send({
        error: error
      });
    }
    User.findById(decoded, function (err, user) {
      if (!err && user) {
        if (!user.emailConfirmed && !user.emailConfirmationSent)
          userController.confirmEmailOfUser(user);
        req.user = user;
        next();
      } else {
        res.send({
          error: {
            status: 401,
            msg: 'Invalid token'
          }
        });
      }
    });
  });
}

function compareVersion(cV, oV)
{
  if (!cV || !oV)
    return false;
  let c = cV.split('.');
  let o = oV.split('.');
  if (c.length !== 3 || o.length !== 3)
    return false;
  for (i of c)
  {
    if (isNaN(i))
      return false;
  }
  for (i of o)
  {
    if (isNaN(i))
      return false;
  }
  let cMajor = parseInt(c[0])*10000;
  let cMinor = parseInt(c[1])*100;
  let cPatch = parseInt(c[2]);
  let oMajor = parseInt(o[0])*10000;
  let oMinor = parseInt(o[1])*100;
  let oPatch = parseInt(o[2]);
  return cMajor + cMinor + cPatch >= oMajor + oMinor + oPatch;
}

function precisionRound(number, precision) {
  var factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
}

module.exports = router;
