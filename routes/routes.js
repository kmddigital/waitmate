var express = require('express');
var router = express.Router();
var passport = require('passport');
// var app = require('../app');

// var logger = app.locals.logger;

// Passport OAuth strategies
require('../config/passport');

var userController = require('../controllers/user');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Home',
    description: 'Get restaurant waiting times submitted by users such as yourself. Spend less time waiting and more time eating.',
    customjs: 'index.js?v=15082017',
    googlemaps: true
  });
});

/* GET tos page. */
router.get('/tos', function (req, res, next) {
  res.render('tos', {
    title: 'Terms Of Service'
  });
});

/* GET privacy page. */
router.get('/privacy', function (req, res, next) {
  res.render('privacy', {
    title: 'Privacy Policy'
  });
});

/* GET app page. */
router.get('/app', function (req, res, next) {
  res.render('app', {
    blankPage: true
  });
});

/* GET support page. */
router.get('/support', function (req, res, next) {
  res.redirect(302, 'mailto:contact@waitmate.io');
});

/* Auth */
router.get('/account', userController.ensureAuthenticated, userController.accountGet);
router.put('/account', userController.ensureAuthenticated, userController.accountPut);
router.delete('/account', userController.ensureAuthenticated, userController.accountDelete);
router.get('/signup', userController.signupGet);
router.post('/signup', userController.signupPost);
router.get('/login', userController.loginGet);
router.post('/login', userController.loginPost);
router.get('/forgot', userController.forgotGet);
router.post('/forgot', userController.forgotPost);
router.get('/reset/:token', userController.resetGet);
router.post('/reset/:token', userController.resetPost);
router.get('/confirm/:token', userController.confirmGet);
router.get('/logout', userController.logout);
router.get('/unlink/:provider', userController.ensureAuthenticated, userController.unlink);
router.get('/auth/facebook', passport.authenticate('facebook', {
  scope: ['email', 'user_location']
}));
router.get('/auth/facebook/callback', passport.authenticate('facebook', {
  successRedirect: '/',
  failureRedirect: '/login'
}));
router.get('/auth/google', passport.authenticate('google', {
  scope: 'profile email'
}));
router.get('/auth/google/callback', passport.authenticate('google', {
  successRedirect: '/',
  failureRedirect: '/login'
}));
router.get('/auth/twitter', passport.authenticate('twitter'));
router.get('/auth/twitter/callback', passport.authenticate('twitter', {
  successRedirect: '/',
  failureRedirect: '/login'
}));

module.exports = router;
