var express = require('express');
var router = express.Router();
var Submission = require('../models/Submission');
var User = require('../models/User');
var app = require('../app');

var logger = app.locals.logger;

/* GET admin page. */
router.get('/', function (req, res, next) {
  if (adminAuth(req)) {
    next(); // trigger 404 page
  } else {
    User.count({}, function (err, userCount) {
      if (err) {
        logger.error('Database error on user.count in GET /admin route', err);
        return res.render('error', {
          message: err.message,
          error: err,
          title: 'Error'
        });
      }
      Submission.count({}, function (err, subCount) {
        if (err) {
          logger.error('Database error on submission.count in GET /admin route', err);
          return res.render('error', {
            message: err.message,
            error: err,
            title: 'Error'
          });
        }
        Submission.find({}).limit(5).populate('user').sort('-createdAt').exec(function (err, submissions) {
          if (err) {
            logger.error('Database error on submission.find in GET /admin route', err);
            return res.render('error', {
              message: err.message,
              error: err,
              title: 'Error'
            });
          }
          var options = {
            start: 0,
            order: 'desc',
            limit: Number.MAX_SAFE_INTEGER
          };

          logger.transports.file.query(options, function (err, logs) {
            if (err) {
              logger.error('Logger error on logger.transports.file.query in GET /admin route', err);
              return res.render('error', {
                message: err.message,
                error: err,
                title: 'Error'
              });
            }

            var length = logs.length;
            logs = logs.slice(0, 5);
            if (logger.exceptionHandlers.exceptions) {
              var options = {
                start: 0,
                order: 'desc'
              };

              logger.exceptionHandlers.exceptions.query(options, function (err, exs) {
                if (err) {
                  logger.error('Logger error on logger.exceptionHandlers.exceptions.query in GET /admin route', err);
                  return res.render('error', {
                    message: err.message,
                    error: err,
                    title: 'Error'
                  });
                }

                res.render('admin/admin', {
                  title: 'Home',
                  adminPage: true,
                  numUsers: userCount,
                  numSubmissions: subCount,
                  submissions: submissions,
                  logs: logs,
                  numLogs: length,
                  numExceptions: exs.length
                });
              });
            } else {
              res.render('admin/admin', {
                title: 'Home',
                adminPage: true,
                numUsers: userCount,
                numSubmissions: subCount,
                submissions: submissions,
                logs: logs,
                numLogs: logs.length
              });
            }
          });
        });
      });
    });
  }
});

/* GET admin users page. */
router.get('/users', function (req, res, next) {
  if (adminAuth(req)) {
    next(); // trigger 404 page
  } else {
    User.find({}, function (err, users) {
      if (err) {
        logger.error('Database error on User.find in GET /admin/users route', err);
        return res.render('error', {
          message: err.message,
          error: err,
          title: 'Error'
        });
      }
      res.render('admin/users', {
        title: 'Users',
        adminPage: true,
        users: users,
        customJSRaw: '<script>$(document).ready(function() {$(\'#dataTables\').DataTable({responsive: true, "order": [[ 3, "desc" ]], "lengthMenu": [ [10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"] ], "pageLength": -1});});</script>'
      });
    });
  }
});

/* GET admin submissions page. */
router.get('/submissions', function (req, res, next) {
  if (adminAuth(req)) {
    next(); // trigger 404 page
  } else {
    res.redirect('/admin/submissions/all');
  }
});

/* GET admin submissions all page. */
router.get('/submissions/all', function (req, res, next) {
  if (adminAuth(req)) {
    next(); // trigger 404 page
  } else {
    Submission.find({}).populate('user').exec(function (err, submissions) {
      if (err) {
        logger.error('Database error on Submission.find in GET /admin/submissions/all route', err);
        return res.render('error', {
          message: err.message,
          error: err,
          title: 'Error'
        });
      }
      res.render('admin/submissions', {
        title: 'Active Submissions',
        adminPage: true,
        submissions: submissions,
        customJSRaw: '<script>$(document).ready(function() {$(\'#dataTables\').DataTable({responsive: true, "order": [[ 3, "desc" ]], "lengthMenu": [ [10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"] ], "pageLength": -1});});</script>'
      });
    });
  }
});

/* GET admin submissions active page. */
router.get('/submissions/active', function (req, res, next) {
  if (adminAuth(req)) {
    next(); // trigger 404 page
  } else {
    Submission.find({
      createdAt: {
        $gt: new Date(Date.now() - 5 * 60 * 1000)
      },
      isActive: true
    }).populate('user').exec(function (err, submissions) {
      if (err) {
        logger.error('Database error on submission.find in GET /admin/submissions/active route', err);
        return res.render('error', {
          message: err.message,
          error: err,
          title: 'Error'
        });
      }
      res.render('admin/submissions', {
        title: 'Active Submissions',
        adminPage: true,
        submissions: submissions,
        customJSRaw: '<script>$(document).ready(function() {$(\'#dataTables\').DataTable({responsive: true, "order": [[ 3, "desc" ]], "lengthMenu": [ [10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"] ], "pageLength": -1});});</script>'
      });
    });
  }
});

/* GET admin submissions inactive page. */
router.get('/submissions/inactive', function (req, res, next) {
  if (adminAuth(req)) {
    next(); // trigger 404 page
  } else {
    Submission.find({
      $or: [ { createdAt: { $lt: new Date(Date.now() - 5 * 60 * 1000) } }, { isActive: false } ]
    }).populate('user').exec(function (err, submissions) {
      if (err) {
        logger.error('Database error on submission.find in GET /admin/submission/inactive route', err);
        return res.render('error', {
          message: err.message,
          error: err,
          title: 'Error'
        });
      }
      res.render('admin/submissions', {
        title: 'Active Submissions',
        adminPage: true,
        submissions: submissions,
        customJSRaw: '<script>$(document).ready(function() {$(\'#dataTables\').DataTable({responsive: true, "order": [[ 3, "desc" ]], "lengthMenu": [ [10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"] ], "pageLength": -1});});</script>'
      });
    });
  }
});

/* GET admin users page. */
router.get('/users/:user/', function (req, res, next) {
  if (adminAuth(req)) {
    next(); // trigger 404 page
  } else {
    User.findById(req.params.user, function (err, user) {
      if (err) {
        logger.error('Database error on user.findById in GET /admin/user/:user route', err);
        return res.render('error', {
          message: err.message,
          error: err,
          title: 'Error'
        });
      }
      res.render('admin/user', {
        title: user.name,
        ouser: user,
        adminPage: true
      });
    });
  }
});

/* POST admin users page. */
router.post('/users/:user', function (req, res, next) {
  if (adminAuth(req)) {
    next(); // trigger 404 page
  } else {
    if ('password' in req.body) {
      req.assert('password', 'Password must be at least 8 characters long').len(8);
      req.assert('confirm', 'Passwords must match').equals(req.body.password);
    } else {
      req.assert('email', 'Email is not valid').isEmail();
      req.assert('email', 'Email cannot be blank').notEmpty();
      req.sanitize('email').normalizeEmail({
        remove_dots: false
      });
    }

    var errors = req.validationErrors();

    if (errors) {
      req.flash('error', errors);
      return res.redirect('/admin/users/' + req.params.user);
    }

    User.findById(req.params.user, function (err, user) {
      if (err) {
        logger.error('Database error on user.findById in POST /admin/users/:user', err);
        return res.render('error', {
          message: err.message,
          error: err,
          title: 'Error'
        });
      }
      if ('password' in req.body) {
        user.password = req.body.password;
      } else {
        user.email = req.body.email;
        user.name = req.body.name;
        user.gender = req.body.gender;
        user.location = req.body.location;
        user.website = req.body.website;
        user.isAdmin = req.body.isadmin;
      }
      user.save(function (err) {
        if ('password' in req.body) {
          req.flash('success', {
            msg: 'Password changed.'
          });
        } else if (err && err.code === 11000) {
          req.flash('error', {
            msg: 'The email address you have entered is already associated with another account.'
          });
        } else if (err) {
          logger.error('Database error on user.save in POST /admin/users/:user', err);
          req.flash('error', {
            msg: 'An unknown error occured.'
          });
        } else {
          req.flash('success', {
            msg: 'Profile information updated.'
          });
        }
        res.redirect('/admin/users/' + req.params.user);
      });
    });
  }
});

/* DELETE admin users page. */
router.delete('/users/:user', function (req, res, next) {
  if (adminAuth(req)) {
    next(); // trigger 404 page
  } else {
    if (req.params.user === req.user.id) {
      req.flash('error', {
        msg: 'You can\'t delete your own account from this page.'
      });
      res.redirect('/admin/users/' + req.params.user);
    } else {
      User.remove({
        _id: req.params.user
      }, function (err) {
        if (err) {
          logger.error('Database error on user.remove in DELETE /admin/users/:user', err);
          return res.render('error', {
            message: err.message,
            error: err,
            title: 'Error'
          });
        }
        Submission.remove({
          user: req.params.user
        }, function (err) {
          if (err) {
            logger.error('Database error on submission.remove in DELETE /admin/users/:user', err);
            return res.render('error', {
              message: err.message,
              error: err,
              title: 'Error'
            });
          }
          req.flash('success', {
            msg: 'This account has been permanently deleted.'
          });
          res.redirect('/admin/');
        });
      });
    }
  }
});

/* GET admin logs page. */
router.get('/logs', function (req, res, next) {
  if (adminAuth(req)) {
    next(); // trigger 404 page
  } else {
    var options = {
      start: 0,
      order: 'desc',
      limit: Number.MAX_SAFE_INTEGER
    };

    logger.transports.file.query(options, function (err, logs) {
      if (err) {
        logger.error('Logger error on logger.transports.file.query in GET /admin/logs', err);
        return res.render('error', {
          message: err.message,
          error: err,
          title: 'Error'
        });
      }
      for (let i = 0; i < logs.length; i++) {
        let copy = JSON.parse(JSON.stringify(logs[i]));
        delete copy.message;
        delete copy.level;
        delete copy.timestamp;
        logs[i].metadata = copy;
      }
      res.render('admin/logs', {
        title: 'Logs',
        adminPage: true,
        logs: logs,
        customJSRaw: '<script>$(document).ready(function() {$(\'#dataTables\').DataTable({responsive: true, "order": [[ 2, "desc" ]], "lengthMenu": [ [10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"] ], "pageLength": -1});});</script>'
      });
    });
  }
});

/* GET admin exceptions page. */
router.get('/exceptions', function (req, res, next) {
  if (adminAuth(req)) {
    next(); // trigger 404 page
  } else if (logger.exceptionHandlers.exceptions) {
    var options = {
      start: 0,
      order: 'desc',
      limit: Number.MAX_SAFE_INTEGER
    };

    logger.exceptionHandlers.exceptions.query(options, function (err, exceptions) {
      if (err) {
        logger.error('Logger error on logger.transports.file.query in GET /admin/exceptions', err);
        return res.render('error', {
          message: err.message,
          error: err,
          title: 'Error'
        });
      }
      res.render('admin/exceptions', {
        title: 'Exceptions',
        adminPage: true,
        exceptions: exceptions,
        customJSRaw: '<script>$(document).ready(function() {$(\'#dataTables\').DataTable({responsive: true, "order": [[ 2, "desc" ]], "lengthMenu": [ [10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"] ], "pageLength": -1});});</script>'
      });
    });
  } else {
    res.render('admin/exceptions', {
      title: 'Exceptions',
      adminPage: true,
      exceptions: [],
      customJSRaw: '<script>$(document).ready(function() {$(\'#dataTables\').DataTable({responsive: true, "order": [[ 2, "desc" ]], "lengthMenu": [ [10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"] ], "pageLength": -1});});</script>'
    });
  }
});

function adminAuth (req) {
  User.find({
    isAdmin: true
  }).exec(function (error, users) {
    if (error) {
      logger.error('Database error on user.find in adminAuth', error);
      /* TODO: Fix */
      /* return res.render('error', {
        message: error.message,
        error: error,
        title: 'Error'
      }); */
    }
    if (isUndf(users) || isEmpty(users)) {
      console.log('No admin users found, making current user (ID: ' + req.user.id + ') an admin.');
      User.findById(req.user.id, function (err, user) {
        if (err) {
          logger.error('Database error on user.findById in adminAuth', err);
        }
        user.isAdmin = true;
        user.save(function (err) {
          if (err) {
            logger.error('Database error on user.save in adminAuth', err);
          }
        });
      });
    }
  });
  return !req.user || !req.user.isAdmin;
}

function isEmpty (obj) {
  return (Object.prototype.toString.call(obj) === '[object Array]' && obj.length === 0) || obj === '';
}

function isUndf (obj) {
  return obj === undefined || obj === null;
}

module.exports = router;
